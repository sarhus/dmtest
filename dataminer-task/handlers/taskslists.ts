import { Context, APIGatewayEvent, APIGatewayProxyResult } from "aws-lambda";
import { QueryResult } from "pg";
import client from "../db";
import { CREATED, success, noContent, OK, notFound } from "../lib/response";
export interface TasksLists {
  id: string;
  task_id: string;
  list_id: string;
}

export async function put(
  event: APIGatewayEvent,
  context: Context
): Promise<APIGatewayProxyResult> {
  const { task_id, list_id } = event.pathParameters;
  await client.connect();

  const check: QueryResult<TasksLists> = await client.query(
    `SELECT * FROM taskslists WHERE task_id = $1 AND list_id = $2;`,
    [task_id, list_id]
  );

  // Task has been already added to the list
  if (check.rows.length > 0) {
    return noContent();
  }

  const res: QueryResult<TasksLists> = await client.query(
    `INSERT INTO taskslists("task_id", "list_id") VALUES($1, $2) RETURNING *`,
    [task_id, list_id]
  );

  const group: TasksLists = res.rows[0];
  return success(CREATED, group);
}

export async function destroy(
  event: APIGatewayEvent,
  context: Context
): Promise<APIGatewayProxyResult> {
  const { task_id, list_id } = event.pathParameters;
  await client.connect();
  await client.query(
    `DELETE FROM taskslists WHERE task_id = $1 AND list_id = $2;`,
    [task_id, list_id]
  );

  return noContent();
}

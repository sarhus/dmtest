import { Context, APIGatewayEvent, APIGatewayProxyResult } from "aws-lambda";
import { QueryResult } from "pg";
import client from "../db";
import { OK, CREATED, success, noContent, notFound } from "../lib/response";
export interface List {
  id: string;
  title: string;
  updatedAt: string;
}

export async function get(
  event: APIGatewayEvent,
  context: Context
): Promise<APIGatewayProxyResult> {
  const id = event.pathParameters.id;

  await client.connect();
  const res: QueryResult<List> = await client.query(
    `SELECT * FROM lists WHERE id = $1;`,
    [id]
  );

  const list: List = res.rows[0];
  return list ? success(OK, list) : notFound();
}

export async function post(
  event: APIGatewayEvent,
  context: Context
): Promise<APIGatewayProxyResult> {
  const { title } = JSON.parse(event.body);
  const updatedAt = new Date().toISOString();

  await client.connect();
  const res: QueryResult<List> = await client.query(
    `INSERT INTO lists("title", "updatedAt") VALUES($1, $2) RETURNING *`,
    [title, updatedAt]
  );

  const list: List = res.rows[0];
  return success(CREATED, list);
}

export async function destroy(
  event: APIGatewayEvent,
  context: Context
): Promise<APIGatewayProxyResult> {
  const id = event.pathParameters.id;

  await client.connect();
  await client.query(`DELETE FROM lists WHERE id = $1;`, [id]);
  return noContent();
}

export async function put(
  event: APIGatewayEvent,
  context: Context
): Promise<APIGatewayProxyResult> {
  const id = event.pathParameters.id;
  const { title } = JSON.parse(event.body);
  const updatedAt = new Date().toISOString();

  await client.connect();
  const res: QueryResult<List> = await client.query(
    `UPDATE lists SET title = $1, "updatedAt" = $2 WHERE id = $3 RETURNING *`,
    [title, updatedAt, id]
  );

  const list: List = res.rows[0];
  return success(OK, list);
}

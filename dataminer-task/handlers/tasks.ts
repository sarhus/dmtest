import { Context, APIGatewayEvent, APIGatewayProxyResult } from "aws-lambda";
import { QueryResult } from "pg";
import client from "../db";
import { OK, CREATED, success, noContent, notFound } from "../lib/response";
export interface Task {
  id: string;
  title: string;
  updatedAt: string;
}

export async function get(
  event: APIGatewayEvent,
  context: Context
): Promise<APIGatewayProxyResult> {
  const id = event.pathParameters.id;

  await client.connect();
  const res: QueryResult<Task> = await client.query(
    `SELECT * FROM tasks WHERE id = $1;`,
    [id]
  );

  const task: Task = res.rows[0];
  return task ? success(OK, task) : notFound();
}

export async function post(
  event: APIGatewayEvent,
  context: Context
): Promise<APIGatewayProxyResult> {
  const { title } = JSON.parse(event.body);
  const updatedAt = new Date().toISOString();

  await client.connect();
  const res: QueryResult<Task> = await client.query(
    `INSERT INTO tasks("title", "updatedAt") VALUES($1, $2) RETURNING *`,
    [title, updatedAt]
  );

  const task: Task = res.rows[0];
  return success(CREATED, task);
}

export async function destroy(
  event: APIGatewayEvent,
  context: Context
): Promise<APIGatewayProxyResult> {
  const id = event.pathParameters.id;

  await client.connect();
  await client.query(`DELETE FROM tasks WHERE id = $1;`, [id]);
  return noContent();
}

export async function put(
  event: APIGatewayEvent,
  context: Context
): Promise<APIGatewayProxyResult> {
  const id = event.pathParameters.id;
  const { title } = JSON.parse(event.body);
  const updatedAt = new Date().toISOString();

  await client.connect();
  const res: QueryResult<Task> = await client.query(
    `UPDATE tasks SET title = $1, "updatedAt" = $2 WHERE id = $3 RETURNING *`,
    [title, updatedAt, id]
  );

  const task: Task = res.rows[0];
  return success(OK, task);
}

The basic project was based on the following package:

https://github.com/AnomalyInnovations/serverless-typescript-starter

# Install

## DB setup

Assuming there's a postgres db running, create db:

`createdb dataminrtest`

Add tables with:

`psql -h localhost -d dataminrtest -f db/structure.sql`

## App setup

Assume there's NodeJS v12 and npm 6.14

Install required packages with:
`npm i`

Run tests with:
`npm t`

It's possible to run locally using serverless offline:

`sls offline`

A local server should hopefully(!) start a list of endpoint would appear:

```
GET    | http://localhost:3000/dev/tasks/{id}
POST   | http://localhost:3000/dev/tasks
DELETE | http://localhost:3000/dev/tasks/{id}
...
```

For example, using postman or curl we can add a Task:

```
  curl --request POST 'http://localhost:3000/dev/tasks' \
  --header 'Content-Type: application/json' \
  --data-raw '{
      "title": "example of a task"
  }'
```

Similarly, we can add a list:

```
  curl --request POST 'http://localhost:3000/dev/lists' \
  --header 'Content-Type: application/json' \
  --data-raw '{
      "title": "example of a list"
  }'
```

We can add a task to a list with:

```
  curl --request PUT 'http://localhost:3000/dev/tasks/1/lists/1' \
  --header 'Content-Type: application/json' \
  --data-raw '{
      "title": "example"
  }'
```

We can also delete a task from a list:

```
  curl --request DELETE 'http://localhost:3000/dev/tasks/1/lists/1'
```

# Comments

I've used 3 hours but there are few things left to do, here's a summary:

1. I'm not checking the validity of the payload, in a real app something like Swagger/OpenAPI would be a good idea

2. I've grouped together lists and tasks handlers in the same file for convenience and simplicity but of course they could be split into separated files (we shouldn't redeploy the entire CRUD endpoint if only one has changed)

3. Unit tests provide a good baseline but the connection with DB is mocked. I should have also written some integration tests: For example have the app running, and then write a test that will hit the URL and make sure the data would be persisted correctly in DB

4. Refactor more: Tasks and Lists handlers are very similar if not almost identical, that could be refactored too

5. Access to DB is sprinkled in the handlers with direct call to `INSERT` or `SELECT`. It would have been nicer to have those separated and isolated in db/index.ts

6. Testing more cases: I've decided to test only for GET endpoints when for 404, the rest (no pun intended!) of scenarios are not handled or tested.

Thanks for reading

CREATE TABLE tasks (
  "id"              SERIAL PRIMARY KEY,
  "title"           VARCHAR(100) NOT NULL,
  "updatedAt"       TIMESTAMP NOT NULL
);
CREATE TABLE lists (
  "id"              SERIAL PRIMARY KEY,
  "title"           VARCHAR(100) NOT NULL,
  "updatedAt"       TIMESTAMP NOT NULL
);
CREATE TABLE taskslists (
  "id"              SERIAL PRIMARY KEY,
  "task_id"         INTEGER,
  "list_id"         INTEGER
);

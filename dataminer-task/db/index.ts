import { Client } from "pg";

const client = new Client({
  host: "localhost",
  database: "dataminrtest",
});

export default client;

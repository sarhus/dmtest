import { APIGatewayEvent, Context } from "aws-lambda";
import * as handler from "../handlers/lists";
import { List } from "../handlers/lists";
import client from "../db";

const id = "1";
const title = "a";
const updatedAt = new Date().toISOString();
const mockList: List = { id, title, updatedAt };

describe("get", () => {
  it("should return a response with status 200 when the list is found", async () => {
    jest.spyOn(client, "connect").mockImplementation(() => {});
    jest.spyOn(client, "query").mockImplementation(() => ({
      rows: [mockList],
    }));

    const event = { pathParameters: { id: "1" } } as unknown as APIGatewayEvent;
    const context = {} as Context;
    const response = await handler.get(event, context);

    expect(response.statusCode).toEqual(200);
    expect(typeof response.body).toBe("string");
    expect(JSON.parse(response.body)).toEqual(mockList);
  });

  it("should return a response with status 404 when the list is not found", async () => {
    jest.spyOn(client, "connect").mockImplementation(() => {});
    jest.spyOn(client, "query").mockImplementation(() => ({
      rows: [],
    }));

    const event = { pathParameters: { id: "1" } } as unknown as APIGatewayEvent;
    const context = {} as Context;
    const response = await handler.get(event, context);

    expect(response.statusCode).toEqual(404);
    expect(typeof response.body).toBe("string");
    expect(response.body).toEqual("Not Found");
  });
});

describe("post", () => {
  it("should return a response with status 201", async () => {
    jest.spyOn(client, "connect").mockImplementation(() => {});
    jest.spyOn(client, "query").mockImplementation(() => ({
      rows: [mockList],
    }));

    const payload = { title };
    const event = { body: JSON.stringify(payload) } as APIGatewayEvent;
    const context = {} as Context;
    const response = await handler.post(event, context);

    expect(response.statusCode).toEqual(201);
    expect(typeof response.body).toBe("string");
    expect(JSON.parse(response.body)).toEqual(mockList);
  });
});

describe("destroy", () => {
  it("should return a response with status 204 ", async () => {
    jest.spyOn(client, "connect").mockImplementation(() => {});
    jest.spyOn(client, "query").mockImplementation(() => ({
      rows: [mockList],
    }));

    const event = { pathParameters: { id: "1" } } as unknown as APIGatewayEvent;
    const context = {} as Context;
    const response = await handler.destroy(event, context);

    expect(response.statusCode).toEqual(204);
    expect(typeof response.body).toBe("string");
    expect(response.body).toEqual("No Content");
  });
});

describe("put", () => {
  it("should return a response with status 200", async () => {
    jest.spyOn(client, "connect").mockImplementation(() => {});
    jest.spyOn(client, "query").mockImplementation(() => ({
      rows: [mockList],
    }));

    const payload = { title };
    const event = {
      pathParameters: { id: "1" },
      body: JSON.stringify(payload),
    } as unknown as APIGatewayEvent;
    const context = {} as Context;
    const response = await handler.put(event, context);

    expect(response.statusCode).toEqual(200);
    expect(typeof response.body).toBe("string");
    expect(JSON.parse(response.body)).toEqual(mockList);
  });
});

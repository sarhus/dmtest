import { APIGatewayEvent, Context } from "aws-lambda";
import * as handler from "../handlers/tasks";
import { Task } from "../handlers/tasks";
import client from "../db";

const id = "1";
const title = "a";
const updatedAt = new Date().toISOString();
const mockTask: Task = { id, title, updatedAt };

describe("get", () => {
  it("should return a response with status 200 when the task is found", async () => {
    jest.spyOn(client, "connect").mockImplementation(() => {});
    jest.spyOn(client, "query").mockImplementation(() => ({
      rows: [mockTask],
    }));

    const event = { pathParameters: { id: "1" } } as unknown as APIGatewayEvent;
    const context = {} as Context;
    const response = await handler.get(event, context);

    expect(response.statusCode).toEqual(200);
    expect(typeof response.body).toBe("string");
    expect(JSON.parse(response.body)).toEqual(mockTask);
  });

  it("should return a response with status 404 when the task is not found", async () => {
    jest.spyOn(client, "connect").mockImplementation(() => {});
    jest.spyOn(client, "query").mockImplementation(() => ({
      rows: [],
    }));

    const event = { pathParameters: { id: "1" } } as unknown as APIGatewayEvent;
    const context = {} as Context;
    const response = await handler.get(event, context);

    expect(response.statusCode).toEqual(404);
    expect(typeof response.body).toBe("string");
    expect(response.body).toEqual("Not Found");
  });
});

describe("post", () => {
  it("should return a response with status 201", async () => {
    jest.spyOn(client, "connect").mockImplementation(() => {});
    jest.spyOn(client, "query").mockImplementation(() => ({
      rows: [mockTask],
    }));

    const payload = { title };
    const event = { body: JSON.stringify(payload) } as APIGatewayEvent;
    const context = {} as Context;
    const response = await handler.post(event, context);

    expect(response.statusCode).toEqual(201);
    expect(typeof response.body).toBe("string");
    expect(JSON.parse(response.body)).toEqual(mockTask);
  });
});

describe("destroy", () => {
  it("should return a response with status 204 ", async () => {
    jest.spyOn(client, "connect").mockImplementation(() => {});
    jest.spyOn(client, "query").mockImplementation(() => ({
      rows: [mockTask],
    }));

    const event = { pathParameters: { id: "1" } } as unknown as APIGatewayEvent;
    const context = {} as Context;
    const response = await handler.destroy(event, context);

    expect(response.statusCode).toEqual(204);
    expect(typeof response.body).toBe("string");
    expect(response.body).toEqual("No Content");
  });
});

describe("put", () => {
  it("should return a response with status 200", async () => {
    jest.spyOn(client, "connect").mockImplementation(() => {});
    jest.spyOn(client, "query").mockImplementation(() => ({
      rows: [mockTask],
    }));

    const payload = { title };
    const event = {
      pathParameters: { id: "1" },
      body: JSON.stringify(payload),
    } as unknown as APIGatewayEvent;
    const context = {} as Context;
    const response = await handler.put(event, context);

    expect(response.statusCode).toEqual(200);
    expect(typeof response.body).toBe("string");
    expect(JSON.parse(response.body)).toEqual(mockTask);
  });
});

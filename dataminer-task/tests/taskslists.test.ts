import { APIGatewayEvent, Context } from "aws-lambda";
import * as handler from "../handlers/taskslists";
import client from "../db";

const mockTaskList = { task_id: "1", list_id: "2" };
describe("put", () => {
  it("should return a response with status 204 when the task is already in the list", async () => {
    jest.spyOn(client, "connect").mockImplementationOnce(() => {});
    jest.spyOn(client, "query").mockImplementationOnce(() => ({
      rows: [mockTaskList],
    }));

    const event = {
      pathParameters: mockTaskList,
      body: {},
    } as unknown as APIGatewayEvent;
    const context = {} as Context;
    const response = await handler.put(event, context);

    expect(response.statusCode).toEqual(204);
    expect(typeof response.body).toBe("string");
    expect(response.body).toEqual("No Content");
  });

  it("should return a response with status 201 when the task is added to the list", async () => {
    jest.spyOn(client, "connect").mockImplementationOnce(() => {});
    jest.spyOn(client, "query").mockImplementationOnce(() => ({
      rows: [],
    }));
    jest.spyOn(client, "query").mockImplementationOnce(() => ({
      rows: [mockTaskList],
    }));

    const event = {
      pathParameters: mockTaskList,
      body: {},
    } as unknown as APIGatewayEvent;
    const context = {} as Context;
    const response = await handler.put(event, context);

    expect(response.statusCode).toEqual(201);
    expect(typeof response.body).toBe("string");
    expect(JSON.parse(response.body)).toEqual(mockTaskList);
  });
});

describe("destroy", () => {
  it("should return a response with status 204", async () => {
    jest.spyOn(client, "connect").mockImplementationOnce(() => {});
    jest.spyOn(client, "query").mockImplementationOnce(() => ({
      rows: [mockTaskList],
    }));

    const event = {
      pathParameters: mockTaskList,
      body: {},
    } as unknown as APIGatewayEvent;
    const context = {} as Context;
    const response = await handler.destroy(event, context);

    expect(response.statusCode).toEqual(204);
    expect(typeof response.body).toBe("string");
    expect(response.body).toEqual("No Content");
  });
});

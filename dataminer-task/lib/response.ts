export const OK = 200;
export const CREATED = 201;
export const NOT_FOUND = 404;
export const NO_CONTENT = 204;

export const success = (statusCode: number, obj: any) => ({
  statusCode,
  body: JSON.stringify(obj),
});

export const notFound = () => ({
  statusCode: NOT_FOUND,
  body: "Not Found",
});

export const noContent = () => ({
  statusCode: NO_CONTENT,
  body: "No Content",
});
